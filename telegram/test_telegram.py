import json, requests, os


BOT_TOKEN = os.environ['TELEGRAM_BOT_TOKEN'] 

url = 'https://api.telegram.org/bot' + BOT_TOKEN


# setWebHook, setea que endpoint se va a usar para notificar los cambios en el chat
payload = {'url': 'https://integracion-redes-gmbarrera455673.codeanyapp.com/hooktelegram' }
response = requests.request("POST", url + '/setWebHook', json=payload)
data = json.loads(response.text)
if data['ok']:
    print(response.text)


# getMe Method, lee la informacion basica del bot
response = requests.request("GET", url + '/getMe')
data = json.loads(response.text)            # Se convierte el Text recibido en Dictionary

if data['ok']:
    print('First Name: ' + data['result']['first_name'])
    print('Username:   ' + data['result']['username'])
else:
    print('Bot not found.')

print('-' * 50)


# getUpdates
response = requests.request("GET", url + '/getUpdates')
data = json.loads(response.text)
if data['ok']:
    for m in data['result']:
        print(str(m['message']['chat']['id']) + ' / ' + m['message']['from']['first_name'] + ': ' + m['message']['text'])

print('-' * 50)


# sendMessage
message = input('Ingrese mensaje: ')
if message != '':
    payload = {'chat_id': '232485752', 'text': message }
    headers = {
        'Content-Type': "application/json",
        }

    response = requests.request("POST", url + '/sendMessage', json=payload, headers=headers)
    data = json.loads(response.text)
    if data['ok']:
        print(response.text)

print('-' * 50)


# getChat
response = requests.request("GET", url + '/getChat')
data = json.loads(response.text)
if data['ok']:
    for m in data['result']:
        print(str(m['message']['chat']['id']) + ' / ' + m['message']['from']['first_name'] + ': ' + m['message']['text'])

print('-' * 50)


