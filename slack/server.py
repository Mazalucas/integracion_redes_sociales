from flask import Flask, request
import json

from client import send_msg

app = Flask(__name__)

@app.route('/slack', methods=['POST'])
def slack_event():

    if request.json['type'] == 'url_verification':
        response = {'challenge': request.json['challenge'] }
        return json.dumps(response)

    if request.json['event']['type'] == 'message':
        emisor = request.json['event']['user']
        canal = request.json['event']['channel']
        texto = request.json['event']['text']

        send_msg('como va?' + emisor + ' - ' + texto, canal)

    return ''

