from flask import Flask, request
import json
import requests

bot_token = 'SU BOT TOKEN'

app = Flask(__name__)
app.config['TESTING'] = True
app.config['DEVELOPMENT'] = True
app.config['DEBUG'] = True


@app.route('/hooktelegram', methods=['POST', 'GET'])
def hook_telegram():
  data = request.json
  print(data['message']['text'])
  
  # sendMessage
  message = input('Ingrese mensaje: ')
  if message != '':
      payload = {'chat_id': data['message']['chat']['id'] , 'text': message }
      headers = {
          'Content-Type': "application/json",
          }

      response = requests.request("POST", 'https://api.telegram.org/bot' + bot_token + '/sendMessage', json=payload, headers=headers)
      data = json.loads(response.text)
      if data['ok']:
          print(response.text)

  print('-' * 50)
  
  return '201'

if __name__ == "__main__":
  app.run('0.0.0.0', port=3000)